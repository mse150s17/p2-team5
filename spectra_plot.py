import matplotlib.pyplot as plt
import numpy as np
import sys

#Data imports 
data = np.loadtxt(sys.argv[1], 'str' ,comments='#',delimiter=',',converters=None, skiprows=0, usecols=(0,1), unpack=True)

#Stuff = np.zeros((2, len(data[1,:])))
Stuff = np.zeros((2,713))
for i in range(713):
        Stuff[1,i] = float(data[1,i][2:-1])

for j in range(713):
        Stuff[0,j] = float(data[0,j][2:-1])

for k in range(713):
        plt.scatter(Stuff[0,k],Stuff[1,k],c='b')

plt.show()

