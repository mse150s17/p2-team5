# Project 2

This repository has some template data files and starter code for our Spring 2017 project #2.

The current example code can be invoked with:

$ python plot.py spectra/Sp15_245L_sect-001_group-2-4_spectrum-H2

and it currently will read in the spectrum data file into a numpy array, and verify it has done so as expected by printing it out.

The purpose of this program is to create a code with the ability to extract all of the data contained in each file. The code skips over the header data which contains information such as the speciman and different parameters, then collects the numeric data, delimits the data based on the locations of the commas. The code then assigns the data to 
its appropriate axis in order for the data to be plotted in graph form with an x and y-axis. The code assigns the graph a header that corresponds
to what is being plotted on the graph as well as axis labels that label the x and y-axis. 

The procedure for plotting a desired set of data is as follows:

1) Run "python bend_spectrum_plot.py"
2) You will be prompted to select "1" or "2" for either a bend or spectrum plot, respectively.
	At this point enter 1 or 2 bases on which data you wish you access.
3) Once a selection is made, you will be asked to enter a filename and a directory.
	#This portion of the code is very important. It is crucial that you enter in the following promts accurately. 
	For example, you would do the following:
		~/mse150/p2-team5/Project2-datafiles/aluminum1.csv
	# Meaning behind each section of the example above-	~/arbitrary/necessary/necessary/necessary/arbitrary.csv
4) Next you will be prompted to enter the filename with the an extension of '.xlsx'
	For example, you would do the following:
		aluminum1.csv.xlsx
5) This would plot the data for the aluminum1.csv file which was selected from the Project2-datafiles folder.
6) It will read in file data
7) Data will be plot in a graph
        The graph will include:
                                Title heading
                                Axis tiles
				Data plotted based on the file chosen as well as the type of data selected i.e. bending/spectra
				Bend plot will graph as Stress (MPa) versus Strain
				Spectra plot will graph as intensity versus wavelength
