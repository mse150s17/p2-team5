#Importing the necessary modules for the code to perform properly.
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os 

def spectrum(): #Controls the reading and parsing of the spectrum esv files
   filename = input("PLease enter filename with directory: ") #Collects the user import of the source esv file
   destination = input('Please enter desired file name ".xlsx" : ') #Collects the user imput for the excel file
   cwd = os.getcwd()#This is the variable that pulls up the current working directory for storing the ecel file
   df =pd.read_csv(filename,header=None) #This line is the one that actually reads in the csv file and converts it to a dataframe
   df.columns=['Wavelength','Intensity']#This names the colums
   df.plot(x='Wavelength',y='Intensity') #What controls the graph
   plt.title('Absorbance Spectrum') 
   plt.ylabel('Intensity (A.U)')
   plt.xlabel('Wavelength (nm)')
   plt.show()
   writer = pd.ExcelWriter(cwd+'/'+destination, engine='xlsxwriter')#This actually writes the excel file, and tells it where it'll be stored
   df.to_excel(writer, sheet_name='Sheet1')#sends df to excel writer
   writer.save()#Outputs and saves excel file


def bend(): #Controls the reading and parsing of the spectrum esv files
   filename = input("Enter filename with directory: ") #Collects the user imput files
   destination = input("Enter desired file name '.xlsx' : ") #Collects the user imput for the excel file
   cwd = os.getcwd()
   d =pd.read_csv(filename, skiprows=31 ) #The skiprows is what skips over the header in each csv
   df1 = pd.DataFrame(d)
   df1.plot(x='Strain [Exten.] %',y='Stress MPa') #What controls the graph
   plt.title('Stress VS Strain curve')
   plt.ylabel('Stress (MPa)')
   plt.xlabel('Strain')
   plt.show()
   writer = pd.ExcelWriter(cwd+'/'+destination, engine='xlsxwriter')
   df1.to_excel(writer, sheet_name='Sheet1')
   writer.save()

def begin(n): #Collects user imput allows user to select function
	if n == '1':
		bend()
	else:
		spectrum()#This is just and if else function, honestly, it should 1 for bending anything else for spectrum
begin(input('Enter 1 for bending data, or 2 for spectrum data: '))#Collects initial user input

